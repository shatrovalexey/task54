### ЗАДАНИЕ ###
* `https://docs.google.com/document/d/19ALyjhBCYA0xDiH8u33qA_1i4kFwplt4wYqfM2YaGmw/edit#heading=h.c7gmgalmyp0m`

### УСТАНОВКА И ЗАПУСК ###
* `git clone ...` ;
* `./artisan install` ;
* `cp -p .env.example .env` и исправить раздел с mysql ;
* `./artisan migrate` ;
* `./artisan serve`. Можно добавить `--host=...` и `--port=...`, `nohup ...`.

### АВТОР ###
Шатров Алексей <mail@ashatrov.ru>