<?php

namespace ASHatrov\Task54\Facades;

use Illuminate\Support\Facades\Facade;

class Task54 extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'task54';
    }
}
