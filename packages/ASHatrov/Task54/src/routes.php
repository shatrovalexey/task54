<?php
	use \ASHatrov\Task54\Email as Email ;

	/**
	* Проверка email в базе
	*/
	Route::get( '/email/get/{email}' , '\ASHatrov\Task54\Email@get' ) ;

	/**
	* Добавление email в базу
	*/
	Route::get( '/email/put/{email}' , '\ASHatrov\Task54\Email@put' ) ;

	/**
	* Удаление email из базы
	*/
	Route::get( '/email/del/{email}' , '\ASHatrov\Task54\Email@del' ) ;

	/**
	* Валидация на корректный email в качестве аргумента, для основных методов
	*/
	Route::get( '/email/inv/{email}' , '\ASHatrov\Task54\Email@inv' ) ;