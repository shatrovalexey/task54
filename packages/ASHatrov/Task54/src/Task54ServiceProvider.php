<?php

namespace ASHatrov\Task54;

use Illuminate\Support\ServiceProvider;

class Task54ServiceProvider extends ServiceProvider
{
	protected $namespace = null ;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ashatrov');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ashatrov');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

	$this->publishes( [
		__DIR__ . '/../database/migrations/' => database_path( 'migrations' )
	] , 'migrations' ) ;
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/task54.php', 'task54');
	// $this->app->make(‘\Controllers\EmailController’);

        // Register the service the package provides.
        $this->app->singleton('task54', function ($app) {
            return new Task54;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['task54'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/task54.php' => config_path('task54.php'),
        ], 'task54.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/ashatrov'),
        ], 'task54.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/ashatrov'),
        ], 'task54.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/ashatrov'),
        ], 'task54.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
