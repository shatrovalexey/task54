<?php

namespace ASHatrov\Task54 ;

use Validator ;
use Illuminate\Database\Eloquent\Model ;
use Illuminate\Support\Facades\DB ;

/**
* @package Email - модель управления e-mail
*/

class Email extends Model {
	public $timestamps = false;
	protected $table = 'email' ;
	private $rules = [
		'value' => 'required|email'
	] ;

	/**
	* Валидация параметров
	* @param array $data - данные для проверки
	* @return boolean - результат
	*/
	protected function invalidate( $data ) {
		return ! Validator::make( $data , $this->rules )->fails( ) ;
	}

	/**
	* Валидация параметра e-mail
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	private function __inv_email( $email ) {
		return self::invalidate( [
			'value' => $email
		] ) ;
	}

	/**
	* Валидация на корректный email в качестве аргумента, для основных методов
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function inv( $email ) {
		return [
			'value' => self::__inv_email( [
				'value' => $email
			] )
		] ;
	}

	/**
	* Проверка email в базе
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function get( $email ) {
		return  [
			'value' => self::__inv_email( $email ) && self::where( 'value' , $email )->exists( )
		] ;
	}

	/**
	* Добавление email в базу
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function put( $email ) {
		if ( ! self::__inv_email( $email ) ) {
			return  [
				'value' => false
			] ;
		}

		$item = new self( ) ;
		$item->value = $email ;

		return  [
			'value' => $item->save( )
		] ;
	}

	/**
	* Удаление email из базы
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function del( $email ) {
		return  [
			'value' => self::__inv_email( $email ) && self::where( 'value' , $email )->delete( )
		] ;
	}
}