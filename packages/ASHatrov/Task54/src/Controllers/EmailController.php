<?php

namespace ASHatrov\Task54\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ASHatrov\Task54\Email;
use namespace ASHatrov\Task54;

/**
* @package EmailController - контроллер управления e-mail
*/

class EmailController extends Controller {
	/**
	* Возвращает результат по установленному формату
	* @param mixed $data - данные
	* @return array - результат
	*/
	protected function result( $data ) {
		return [ 'result' => $data ] ;
	}

	/**
	* Валидация на корректный email в качестве аргумента, для основных методов
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function inv( $email ) {
		return self::result( Email::inv( $email ) ) ;
	}

	/**
	* Проверка email в базе
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function get( $email ) {
		return self::result( Email::get( $email ) ) ;
	}

	/**
	* Добавление email в базу
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function put( $email ) {
		return self::result( Email::put( $email ) ) ;
	}

	/**
	* Удаление email из базы
	* @param string $email - e-mail
	* @return boolean - результат
	*/
	public function del( $email ) {
		return self::result( Email::del( $email ) ) ;
	}
}