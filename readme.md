### ЗАДАНИЕ ###
https://docs.google.com/document/d/19ALyjhBCYA0xDiH8u33qA_1i4kFwplt4wYqfM2YaGmw/edit#heading=h.c7gmgalmyp0m

### ЗАПУСК ###
* `git clone ...`;
* `composer install`;
* `./artisan migrate`;
* `nohup /.artisan serve --host=${HOST_NAME} --port=${PORT_ID} 2> /dev/null &`.

### АВТОР ###
Шатров Алексей <mail@ashatrov.ru>